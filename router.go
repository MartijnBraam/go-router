package main

import (
	"encoding/json"
	"github.com/vishvananda/netlink"
	"log"
	"net/http"
)

func handler(w http.ResponseWriter, r *http.Request) {
	links, _ := netlink.LinkList()
	json.NewEncoder(w).Encode(links)
}

func main() {
	log.Println("Starting webserver on :8080")

	fs := http.FileServer(http.Dir("static"))
	http.Handle("/static/", http.StripPrefix("/static/", fs))

	http.HandleFunc("/", handler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
