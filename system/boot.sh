#!/bin/sh

echo "Cleaning..."
rm -rf initramfs.cpio
rm -rf initramfs.igz
rm -rf initramfs

echo "Building..."
cd ..
rm -rf router
GOOS=linux GOARCH=arm go build
cd system

echo "Assembling..."
mkdir -p initramfs/bin
cp busybox-armv7l initramfs/bin/busybox
cp ../router initramfs/
cp -r ../static initramfs/static
cp init.sh initramfs/init
mkdir -p initramfs/usr/bin
mkdir -p initramfs/usr/sbin
mkdir -p initramfs/sbin
mkdir -p initramfs/proc
mkdir -p initramfs/sys
mkdir -p initramfs/dev
#cp ../router initramfs/init
chmod -R 777 initramfs

echo "Creating initramfs image..."
cd initramfs
find . | cpio -H newc -o > ../initramfs.cpio
cd ..
cat initramfs.cpio | gzip > initramfs.igz

echo "Launching..."
QEMU_AUDIO_DRV=none qemu-system-arm -machine vexpress-a9 -kernel vmlinuz-3.2.0-4-vexpress -append "init=/router console=ttyAMA0" -initrd initramfs.cpio -serial stdio -netdev user,id=net0,hostfwd=tcp::8080-:8080 -device virtio-net-device,netdev=net0