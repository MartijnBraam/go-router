#!/bin/busybox sh
echo "Entering initramfs"
/bin/busybox --install -s
mknod /dev/null c 1 3
mknod /dev/tty c 5 0

mount -t proc none /proc
mount -t sysfs none /sys

mdev -s
echo "Printing network links..."
ip l
echo .
echo "Starting router..."
./router